const fs = require('fs');

/**
 * Writes chat to a .json file
 * @param {string} id Id of the chatroom
 * @param {JSON} data Chatroom data
 * @return {Promise}
 */
function saveChat(id, data) {
    return new Promise((resolve, reject) => {
        let file = 'chats/' + id + '.json';
        delete data._messages;

        fs.writeFile(file, JSON.stringify(data, null, '\t'), (error, data) => {
            error ? reject(error) : resolve (data);
        });
    });
}

/**
 * Reads .json file of chat
 * @param {string} id Id of the chatroom
 * @return {Promise}
 */
function getChat(id) {
    return new Promise((resolve, reject) => {
        let file = 'chats/' + id + '.json';

        fs.readFile(file, (error, data) => {
            try {
                var chat = JSON.parse(data);

                chat._messages = new Array();

                chat.messages.forEach((message) => {
                    var user = chat.users.find((user) => {
                        return message.identity === user.identity;
                    });

                    if (user === undefined) {
                        user = {
                            identity: message.identity,
                            name: 'Anonymous',
                            color: '#313d33'
                        }
                    }

                    var hours = new Date(message.date).getHours();
                    var minutes = new Date(message.date).getMinutes();
                    message.time = hours + ':' + minutes;

                    chat._messages.push({
                        user: user,
                        body: message.body,
                        time: message.time
                    });
                });

                error ? reject(error) : resolve(chat);
            } catch (error) {
                reject(error);
            }
        });
    });
}

module.exports.saveChat = saveChat;
module.exports.getChat = getChat;