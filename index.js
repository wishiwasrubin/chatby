/**
 * chatby
 * @author Vera Rubin
 * @license MIT
 * Quick messaging channels on demand
 */

/**
 * Express framework
 */ 
const express = require('express');

/**
 * Express app
 */
const app = express();
const http = require('http').createServer(app);

/**
 * Socket IO
 */
const io = require('socket.io')(http);

const favicon = require('serve-favicon');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');

const session = require('express-session');
const SQLite = require('connect-sqlite3')(session);
const sharedsession = require("express-socket.io-session");

require('dotenv').config();

app.set('views', __dirname + '/views');
app.set('view engine', 'pug');


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

var sessionMiddleware = session({
    store: new SQLite,
    secret: process.env.SECRET,
    resave: false,
    saveUninitialized: false,
    cookie: { 
        // 1 week
        maxAge: 7 * 24 * 60 * 60 * 1000 
    } 
});
io.use(sharedsession(sessionMiddleware, {
    autoSave: true
}));
app.use(sessionMiddleware);

app.use(favicon('public/favicon.ico'));    
app.use(express.static(__dirname + '/public'));

const functions = require('./functions');

io.on('connection', (socket) => {
    var room = socket.handshake.query.room;
    socket.join(room);

    socket.on('chat message', (message) => {
        message.identity = socket.handshake.session.identity;

        functions.getChat(room).then((chat) => {
            chat.messages.push(message);
            
            functions.saveChat(room, chat).then(() => {
                return;
            }).catch((error) => {
                console.log(error);
            });

            message.user = chat.users.find((user) => {
                return message.identity === user.identity
            });

            if (message.user === undefined) {
                message.user = {
                    identity: message.identity,
                    name: 'Anonymous',
                    color: '#313d33'
                }
            }

            delete message.identity;

            var hours = new Date(message.date).getHours();
            var minutes = new Date(message.date).getMinutes();
            message.time = hours + ':' + minutes;

            message.socket = socket.id;

            io.in(room).emit('chat message', message);
        }).catch((error) => {
            console.log(error);
        });
    });

});

/**
 * App routing
 */
require('./routes')(app, io);

http.listen(process.env.PORT, () => {
    console.log("chatby is listening on port " + process.env.PORT);
});
