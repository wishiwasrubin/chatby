# chatby
TLDR: I found most instant messaging services to be quite annoying about security and users verification, so I made this simpler chat service using express.js and socket.io.

See it live and chat at [chatby](https://chatby.glitch.me)

## How it works
Chat rooms are identified by a random base36 string paired to a JSON files that acts as chat log.

Users can access chat rooms if they have the link to an existing room. Users are identified using automatic session generations via UUIDs and persitently stored via SQLite.

Messages are coordinated and stored in real time using socket.io.

App works on express.js and is deployed at glitch.com because I personally like glitch, but it's a [12 factor app](https://www.12factor.net) so could be deployed anywhere.

### Known issues
Unknown ¿caching error? leading to the new room generation system to generate an already existing, or once existed, room over and over. No attempts to fix this issue deliver any result as apparently the code for room generation gets executed somewhere else other than chatby stack.