/**
 * Send a notification object to the user browser
 * @param {object} message
 */
function notify(message) {
    if (!("Notification" in window) && (Notification.permission === "granted")) {
        var notification = new Notification('New message', {
            body: message.user.name + ': ' + message.body,
            icon: '/favicon.ico'
        });
    }
    
    else if (Notification.permission !== "denied") {
        Notification.requestPermission().then(function (permission) {
            if (permission === "granted") {
                var notification = new Notification('New message', {
                    body: message.user.name + ': ' + message.body,
                    icon: '/favicon.ico'
                });
            }
        });
    }  
}

$(() => {
    var room = window.location.pathname.split('/')[1];
    var socket = io({query: 'room=' + room });

    // Keep fixed to bottom on page load
    var out = document.getElementById("chatbox");
    out.scrollTop = out.scrollHeight - out.clientHeight;

    $('#mouth').submit((e) => {
        e.preventDefault(); // prevents page reloading
        socket.emit('chat message', {
            body: $('#m').val(),
            date: new Date().getTime()
        });

        $('#m').val('');
        return false;
    });

    socket.on('chat message', (message) => {
        var out = document.getElementById("chatbox");
        var isScrolledToBottom = out.scrollHeight - out.clientHeight <= out.scrollTop + 1;

        $('#messages').append($('<li>').css('background-color', message.user.color)
        .append($('<span>').text(message.user.name + ' '))
        .append($('<span class="date">').text(message.time))
        .append($('<p>').text(message.body)));
        
        if (isScrolledToBottom) {
            out.scrollTop = out.scrollHeight - out.clientHeight;
        }

        if (socket.id !== message.socket) {
            notify(message);
        }
    });

});