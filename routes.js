module.exports = function(app, io) {
    const functions = require('./functions');
    const fs = require('fs');
    const uuid = require('uuid/v1');

    app.get('/', (request, response, next) => {
        if(!request.session.identity) {
            request.session.identity = uuid();
        }

        response.render('index');
    });

    app.get('/!new', (request, response, next) => {
        function newId() {
            let id = Math.random().toString(36).substring(2, 8);

            if (fs.existsSync('chats/' + id + '.json')) {
                newId();
            } else {
                functions.getChat('.chatby').then((chat) => {
                    chat.messages[0].date = new Date().getTime();
                    
                    functions.saveChat(id, chat).then(() => {
                        response.redirect(301, '/' + id);
                    }).catch((error) => {
                        response.render('error', {message: error});
                    });
                }).catch((error) => {
                    response.render('error', {message: error});
                });
            }
        }

        newId();
    });

    app.get('/:id', (request, response, next) => {
        let id = request.params.id;
        
        if(!request.session.identity) {
            request.session.identity = uuid();
        }
        let identity = request.session.identity;

        functions.getChat(id).then((chat) => {
            var user = chat.users.find((user) => {
                return identity === user.identity;
            });

            if (user === undefined) {
                user = {
                    identity: identity,
                    name: 'Anonymous',
                    color: "#"+((1<<24)*Math.random()|0).toString(16)
                }
            }

            response.render('chat', {room: id, messages: chat._messages, user: user});
        }).catch((error) => {
            response.render('error', {message: "This link does not lead to any chat."});
        });
    });

    app.post('/:id/settings', (request, response, next) => {
        let id = request.params.id;
        let identity = request.session.identity;
        
        functions.getChat(id).then((chat) => {
            for(var i = chat.users.length - 1; i >= 0; i--) {
                if(chat.users[i].identity === identity) {
                   chat.users.splice(i, 1);
                }
            }

            chat.users.push({
                identity: identity,
                name: request.body.name,
                color: request.body.color,
            });

            functions.saveChat(id, chat).then(() => {
                response.redirect(301, '/' + id);
            }).catch((error) => {
                response.render('error', {message: error});
            });
        }).catch((error) => {
            response.render('error', {message: "This link does not lead to any chat."});
        });
    });
};
